
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
#include <pwd.h>

#include "file.h"
#include "interpreter.h"
#include "list.h"
#include "pinit.h"
#include "project.h"
#include "utils.h"

static void copy_files_in_folder(char *folder, char* path);
static void copy_file(char *file, char *folder, char *path);
static int copy_binary(FILE *src, FILE *dst);
static int is_binary(char* src_path);

/*
 * Creates a Pinit project in the given folder,
 * based on the files in the template for the
 * language.
 */
void create_project(char *language, char *folder)
{
	// Check if the folder exist.
	struct stat st;
	if(strcmp(folder, ".") != 0)
		if(stat(folder, &st) == -1)
			create_folder(folder);
		

	//Check if the src folder exist, and if not create it.
	char *src_folder = join_strings(2, folder, "/src");
	if(stat(src_folder, &st) == -1)
		create_folder(src_folder);
	free(src_folder);
	copy_files_in_folder(folder, language);
}

static void copy_files_in_folder(char *folder, char* path)
{
	DIR *d;
	struct dirent *dir;
	d = opendir(path);
	if(d) {
		while((dir = readdir(d)) != NULL){
			if(dir->d_name[0] != '.'){
				struct stat st;
				char *new_path = join_strings(2, path, dir->d_name);
				stat(new_path, &st);
				if(st.st_mode & S_IFDIR){
					free(new_path);
					new_path = join_strings(3, path, dir->d_name, "/");
					char *new_folder = join_strings(3, folder, "/", dir->d_name);
					if(stat(new_folder, &st) == -1)
						create_folder(new_folder);
					copy_files_in_folder(new_folder, new_path);
					free(new_folder);
				}	
				else {
					copy_file(dir->d_name, folder, path);
				}
				free(new_path);
			}
		}
		closedir(d);
	} else {
		printf("Failed to open folder %s\n", path);
	}
}

/*
 * Checks if the spesified language is a folder.
 * Returns 1 if it exist.
 */
char* does_language_exist(char *language)
{
    char *path;
    struct stat sb;
    if(CONFIGPATH && !DEFAULT_ONLY) {
        char *p1 = join_strings(3, CONFIGPATH, "makefiles/", language);
		path = join_strings(3, p1, "/", "");
		free(p1);
        if(!stat(path, &sb) && S_ISDIR(sb.st_mode)){
            return path;
        }
        free(path);
    }

	path = join_strings(3, MAKEFILES, language, "/");
	if(stat(path, &sb) == 0 && S_ISDIR(sb.st_mode))
		return path;
	free(path);
	return NULL;
}


/*
 * Copies the content of a file to the new location.
 */
static void copy_file(char *filename, char *folder, char *path)
{
	FILE *src, *dst;

	char *src_path = join_strings(2, path, filename);

	char dst_path[strlen(filename) + strlen(folder) + 3];
	strcpy(dst_path, folder);
	strcat(dst_path, "/");
	if(strcmp(filename, "gitignore") == 0)
		strcat(dst_path, ".");
	strcat(dst_path, filename);

	if(is_binary(src_path) || !strcmp(filename, "gitignore") || !strcmp(filename, "Makefile")){
		src = fopen(src_path, "rb");
		dst = fopen(dst_path, "wb");
		int status = copy_binary(src, dst);
		if(status)
			printf("Error: Failed to copy file %s\n", filename);
	} else {
		src = fopen(src_path, "rb");
		file fl;
		fl.outputname = dst_path;
        char *dash = strrchr(dst_path, '/');
        if(dash)
		    fl.fullname = dash+1;
        else
            fl.fullname = dst_path;
		interpret_file(src, &fl, NULL);	
		fclose(src);
	}
	free(src_path);
}

static int copy_binary(FILE *src, FILE *dst)
{
	unsigned char buffer[8192];
	size_t n, m;
	do {
		n = fread(buffer, sizeof(char), sizeof buffer, src);
		if(n) m = fwrite(buffer, sizeof(char), n, dst);
		else m = 0;
	} while((n > 0)&&(n == m));

	if(m)
		return 1;
	return 0;
}

static int is_binary(char* src_path)
{
	FILE *src = fopen(src_path, "rb");
	if(src){
		int buffer[8192];
		fread(buffer, sizeof(char), sizeof buffer, src);
		fclose(src);
		return memchr(buffer, '\0', 8192) != NULL;
	}
	return 0;
}


