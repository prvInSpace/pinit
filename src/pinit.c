/*
 *	File: pinit.c
 *	
 *	The main source file for the Pinit program.
 *
 *	Author: Preben Vangberg
 *	Date: 10. December 2018
 *
 *	This file is the main source file and entry point of
 *	the program. It defines the general flow of the program
 *	and takes care of the reading of the arguments.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pwd.h>
#include <unistd.h>

#include "file.h"
#include "list.h"
#include "project.h"
#include "utils.h"

/* The name of the program */
#define NAME "pinit"

/* The current version of the program */
#define VERSION "2.4.2"

static int check_arguments(int argc, char **argv, node **files);
static void print_help(void);
static void print_version(void);

/* Standard path for makefiles and templates */
char *TEMPLATE_PATH = "/usr/share/prv/";

/* The standard path of the makefiles */
char *MAKEFILES;

/* The standard path of the templates */
char *TEMPLATES;

/* The default project folder */
char *DEFAULT_PROJECT_FOLDER = ".";

/* The default config folder */
char *CONFIGPATH = "";

/* The project type found in the argument*/
char *PROJECT_TYPE = "";

/* The folder specified, else it will point to the default folder */
char *PROJECT_FOLDER = NULL;

/* Answer yes to every question */
int YES_MAN = 0;

/* Answer no to every question */
int NO_MAN = 0;

/* Sets a flag to use default templates only */
int DEFAULT_ONLY = 0;


int main(int argc, char **argv)
{
	if(argc == 1){
		printf("No arguments provided! Please write pinit --help for more information.\n");
		exit(0);
	}

	// Locate config folder.
	char *homedir;
	if(!(homedir = getenv("HOME"))){
		homedir = getpwuid(getuid())->pw_dir;
	}
	if(homedir){
		CONFIGPATH = join_strings(2, homedir, "/.config/pinit/");
	}

	MAKEFILES = join_strings(2, TEMPLATE_PATH, "makefiles/");
	TEMPLATES = join_strings(2, TEMPLATE_PATH, "templates/");

	node *files = NULL;
	int found_project = check_arguments(argc, argv, &files);

	if(found_project)
		create_project(PROJECT_TYPE, PROJECT_FOLDER);
	if(files) {
		create_files(files);
	}
	if(!files && !found_project)
		printf("Unknown command!\n");

	return 0;
}


/*
 * This function goes though all the arguments provided to pinit
 * upon the start of the program. It sets any flags, and creates
 * a list of the files provided if it finds any.
 *
 * Takes the number of arguments and pointer of the arguments given
 * in the main function.
 *
 * Returns 0 if no project was found, 1 otherwise.
 */
static int check_arguments(int argc, char **argv, node **files)
{
	char* project_path = NULL;
	int returnval = 0;
	for(int i = 1; i < argc; ++i){
		// Help prompt
		if(strcmp(argv[i], "--help") == 0){
			print_help();
			exit(0);
		}

		// Version prompt
		else if(strcmp(argv[i], "--version") == 0){
			print_version();
			exit(0);
		}

		// Use default templates only
		else if(!strcmp(argv[i], "-default")){
			DEFAULT_ONLY = 1;
		}

		// Yesman mode
		else if(!strcmp(argv[i], "-y")){
			if(NO_MAN){
				printf("Error: You can not activate both yesman and noman at the same time!\n");
				exit(1);
			} else {
				YES_MAN = 1;
			}
		}

		// Noman mode
		else if(!strcmp(argv[i], "-n")){
			if(YES_MAN){
				printf("Error: You can not activate both yesman and noman at the same time!\n");
				exit(1);
			} else {
				NO_MAN = 1;
			}
		}

		// Check if arg is a known language
		else if(!project_path && (project_path = does_language_exist(argv[i]))){
			returnval = 1;
			PROJECT_TYPE = project_path;
			if(i + 1 != argc) {
				i++;
				if(check_if_file(argv[i], files))
					PROJECT_FOLDER = DEFAULT_PROJECT_FOLDER;
				else
					PROJECT_FOLDER = argv[i];
			} else {
				PROJECT_FOLDER = DEFAULT_PROJECT_FOLDER;
			}
		}

		// Check if arg is a filename
		else if(check_if_file(argv[i], files));

		// Unknown argument, exits the program.
		else {
			printf("Unknown argumment %s\n", argv[i]);
			exit(1);
		}
	}
	return returnval;
}


/*
 * Prints the help message
 */
static void print_help(void)
{
	char *help[] = {
		"\n                         "NAME" "VERSION"\n",
		"A program which creates projects or file for any given programming",
		"lanuguage. For more information regarding which languages and",
		"extensions are available, please refer to the README.md file.\n",
		"Create a project: pinit [language] [OPTIONAL: Folder]",
		"Create a file: pinit [Filename]...\n",
		"Additional Arguments:\n",
		"-default",
		"   Forces the use of default templates only\n",
		"--help",
		"   Displays this message and exits.\n",
		"-n",
		"   Turns on noman mode. Will automaticly answer no to all questions.\n",
		"--version",
		"   Displays the name and version of the program\n",
		"-y",
		"   Turns on yesman mode. Will automaticly answer yes to all questions.\n"	
	};
	for(int i = 0; i < sizeof(help) / sizeof(help[0]); ++i)
		printf("%s\n", help[i]);
}


/*
 *	Prints the name and version of the program.
 */
static void print_version(void)
{
	char *text[] = {
		NAME " " VERSION
	};
	for(int i = 0; i < sizeof(text) / sizeof(text[0]); ++i)
		printf("%s\n", text[i]);
}



