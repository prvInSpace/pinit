/*
 *	File: file.c
 *	
 *	File.c contains the functions for creation of sourcefiles.
 *
 *	Author: prv
 *	Date: 10/12/2019
 *
 *	This file contains all the functions for the creation of
 *	sourcefiles based on templates. 
 *
 *	The parsing of the template files can be found in template.c
 */

#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include "file.h"
#include "list.h"
#include "interpreter.h"
#include "pinit.h"
#include "utils.h"


static void create_file(file *fl);

static char *get_template_path(char *extension);

static void check_for_redirect(file *fl);

static void check_for_extension(file *fl);

// Names for the different files
char *redirect_name = "/redirect";
char *template_name = "/template";
char *extension_name = "/extension";

/*
 *	This takes the list of files gathered
 *	from the parsing of arguments. It
 *	iterate through the list and creates
 *	the files.
 */
void create_files(node *files) {
    while (files) {
        create_file(files->data);
        files = files->next;
    }
}


/*
 *	Looks up the folder for the given
 *	language for the file. Then it
 *	redirect, or change the extension
 *	if such is defined in the template
 *	folder.
 *	
 *	It creates the folder of the file
 *	if neccesary.
 *
 *	Then it calls the function for
 *	parsing of the template which will
 *	fill the file. 
 */
static void create_file(file *fl) {
    //If a path was spesified see if it exist, else make it.
    if (fl->path) {
        struct stat st;
        if (stat(fl->path, &st) == -1)
            create_folder(fl->path);
    }


    check_for_redirect(fl);
    check_for_extension(fl);

    char *template_file_path = join_strings(2, fl->template_path, template_name);

    // Open the template path and process it.
    FILE *template = fopen(template_file_path, "r");
    if (!template) {
        printf("Warning: No template file found for extension: %s\n", fl->extension);
    } else {
        // Create a list of all the extensions asked for and add the current one.
        node *variables = NULL;
        variable *this = malloc(sizeof(variable));
        this->name = fl->type;
        this->value = 1;
        add_element(&variables, this);

        interpret_file(template, fl, &variables);
        fclose(template);

        // Clear the list
        clear_list(&variables);
    }

    //Deallocating the strings used.
    free(template_file_path);
}


/*
 * Checks if the given string match a file pattern
 * with an extension. If it has an extenion, it creates
 * a file struct containing the data it have found.
 * It adds this struct to the list.
 *
 * Returns 0 if it does not find the spesified
 * language or the file does not contain an
 * extension. 
 *
 * Returns 1 if it finds and extension and it
 * exist a template for the extension.
 */
int check_if_file(char *argv, node **files) {
    // Try to find the last index of '.'
    char *ext = strrchr(argv, '.');

    // If no fot was found, then this is not a file.
    if (!ext)
        return 0;

    //Remove the dot.
    ext++;
    size_t ext_length = strlen(ext);

    // If ext_length is 0 throw an error.
    if (!ext_length) {
        printf("Error: Dot at the end of filename %s\n", argv);
        return 0;
    }

    // Try to find the template
    char *folder_path = get_template_path(ext);

    if (!folder_path)
        return 0;


    file *f = malloc(sizeof(file));
    f->type = malloc((ext_length + 1) * sizeof(char));
    strcpy(f->type, ext);
    f->template_path = folder_path;
    check_for_extension(f);
    if (!f->extension)
        f->extension = f->type;

    if(PROJECT_FOLDER){
        argv = join_strings(3, PROJECT_FOLDER, "/src/", argv);
    }

    //Check if the filename have a prefixed path.
    //If it has then we need to structure the filenames
    //differently
    char *dash = strrchr(argv, '/');
    if (dash) {
        //Minus 2 as we remove . and /
        size_t basename_size = strlen(dash) - ext_length - 2;
        f->basename = malloc(basename_size * sizeof(char));

        char *pointer = dash +1;
        int i;
        for (i = 0; i < basename_size; ++i) {
            f->basename[i] = pointer[i];
        }
        f->basename[i] = 0;

        size_t path_length = (size_t) (dash - argv + 2);
        f->path = malloc(path_length * sizeof(char));
        for (i = 0; i < path_length - 1; i++)
            f->path[i] = argv[i];
        f->path[path_length - 1] = 0;
        f->fullname = join_strings(3, f->basename, ".", f->extension);
        f->outputname = join_strings(2, f->path, f->fullname);
    } else {
        f->path = NULL;
        f->basename = malloc(strlen(argv) - ext_length);
        int i;
        for (i = 0; i < strlen(argv) - ext_length -1; i++)
            f->basename[i] = argv[i];
        f->basename[i] = 0;
        f->fullname = join_strings(3, f->basename, ".", f->extension);
        f->outputname = f->fullname;
    }
#ifdef DEBUG
    printf("File:\n Basename: %s\n Fullname: %s\n Extension: %s\n Path: %s\n", f->basename, f->fullname, f->extension, f->path);
#endif
    add_element(files, f);
    return 1;
}


/*
 *	Prompts the user if it wants
 *	to create a given file.
 *
 *	If the user answers yes, then
 *	it will create the linked file.
 *
 *	Returns 0 if the user answered no.
 *	Returns 1 if the linked file is created.
 *
 */
int link_prompt(file *fl, char *buffer, node **variables) {
    char answer[11] = {0};
    if (!YES_MAN) {
        printf("Do you want to create %s file for %s? [y/N]: ", buffer, fl->fullname);
        fgets(answer, 10, stdin);
    }
    if (YES_MAN || answer[0] == 'y' || answer[0] == 'Y') {
        create_linked_file(fl, buffer, variables);
        return 1;
    }
    return 0;
}


/*
 * This sort of do the same as
 * create file, but the behavior
 * is sort of different.
 *
 * This should probably be cleaned
 * up.
 */
void create_linked_file(file *fl, char *buffer, node **variables) {
    //Create new file
    file newfile;
    newfile.basename = fl->basename;
    newfile.path = fl->path;

    newfile.template_path = get_template_path(buffer);
    if (!newfile.template_path)
        return;

    //Open the right template file and check for redirect.
    check_for_extension(&newfile);
    check_for_redirect(&newfile);

    if (!newfile.extension) {
        newfile.extension = malloc((strlen(buffer) + 1) * sizeof(char));
        strcpy(newfile.extension, buffer);
    }

    if (newfile.path)
        newfile.outputname = join_strings(4, newfile.path, newfile.basename, ".", newfile.extension);
    else
        newfile.outputname = join_strings(3, newfile.basename, ".", newfile.extension);

    newfile.fullname = join_strings(3, newfile.basename, ".", newfile.extension);
    newfile.type = malloc(sizeof(char) * (strlen(buffer) + 1));
    strcpy(newfile.type, buffer);

    //Do the actual work
    char *new_template_file_path = join_strings(2, newfile.template_path, template_name);
    FILE *new_template = fopen(new_template_file_path, "r");
    if (!new_template) {
        printf("Warning: No template file found for extension: %s\n", buffer);
    } else {
        interpret_file(new_template, &newfile, variables);
        fclose(new_template);
    }

    //Remember to deallocate strings
    free(newfile.fullname);
    free(new_template_file_path);
    free(newfile.extension);
}

static char *get_template_path(char *extension) {
    struct stat sb;
    char *folder_path = join_strings(3, CONFIGPATH, "templates/", extension);
    if (stat(folder_path, &sb) || !S_ISDIR(sb.st_mode) || DEFAULT_ONLY) {
        free(folder_path);
        folder_path = join_strings(2, TEMPLATES, extension);
        if (stat(folder_path, &sb) || !S_ISDIR(sb.st_mode)) {
            free(folder_path);
            folder_path = NULL;
        }
    }

    if(folder_path == NULL)
        printf("Error: No template for filetype %s\n", extension);

    return folder_path;
}


/*
 * If there exist an redirect file, than this changes the
 * template_path in the file
 */
static void check_for_redirect(file *fl) {
    //Check if redirect file exist, if so redirect the read folder.
    char *redirect_path = join_strings(2, fl->template_path, redirect_name);
    FILE *redirect = fopen(redirect_path, "r");
    if (redirect) {
        char *source_folder;
        source_folder = malloc(10 * sizeof(char));
        if (!source_folder) {
            printf("Error: Failed to allocate memory");
            exit(1);
        }

        int status = fscanf(redirect, "%9s", source_folder);
        if (status != 1) {
            printf("Error: Failed to redirect file %s\n", fl->fullname);
            return;
        }
        free(fl->template_path);
        fl->template_path = join_strings(3, CONFIGPATH, "templates/", source_folder);
        fclose(redirect);
    }
    free(redirect_path);
}


/*
 * Checks if an extension file exist in the template folder.
 *
 * If it does, then it changes the file extensions.
 * If not, then it changes the extension to NULL;
 */
static void check_for_extension(file *fl) {
    //Try to open the extension file. If it exist, replace the extension
    char *extension_file_path = join_strings(2, fl->template_path, extension_name);
    FILE *extension = fopen(extension_file_path, "r");
    if (extension) {
        char buffer[10];
        int status = fscanf(extension, "%9s", buffer);
        if (status == 1) {
            fl->extension = malloc((strlen(buffer) + 1) * sizeof(char));
            strcpy(fl->extension, buffer);
        } else {
            printf("Error: Unable to read the extension file for .%s.", fl->extension);
            exit(1);
        }
        fclose(extension);
    } else {
        fl->extension = NULL;
    }

    //Free the string
    free(extension_file_path);
}
