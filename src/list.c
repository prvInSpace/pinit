/*
 * File: list.c
 *
 * A very simple implementation of a linked list.
 *
 * Author: Preben Vangberg
 * Date: 10. December 2018
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include "list.h"

static node* create_node(void *element);


/*
 * Adds the element to a the given list.
 */
void add_element(node **head, void *element){
	node *n = create_node(element);
	if(*head)
		n->next = *head;
	*head = n;
}


/*
 * Clears the list, de-allocates the memory
 * for all the nodes and data, and sets the head
 * to NULL.
 */
void clear_list(node **head)
{
	node *p = (*head);
	while(p){
		node *next = (*p).next;
		free(p->data);
		free(p);
		p = next;		
	}	
	*head = NULL;
}


/*
 * Creates a node struct that contains a pointer
 * to some data and the next element in the list.
 *
 * Returns new node.
 */
static node* create_node(void *element)
{
	node *n = malloc(sizeof(node));
	n->next = NULL;
	n->data = element;
	return n;
}

