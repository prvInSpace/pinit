/*
 *	File: interpreter.c
 *	
 *	File for parsing of template files.
 *
 *	Author: prv
 *	Date: 22/01/2019
 *
 *	This file contains the functions for
 *	parsing and interpreting the contents of
 *	the templatefiles.
 *
 *  When it was written, only I and God
 *  knew how it works. In all the excitement
 *  I have lost the track of it myself, so
 *  now only God knows.
 *
 *  If you ever find out you want to refactor
 *  this, and failed, please increment the number
 *  below as a warning to everyone else.
 *
 *  total_hours_wasted: 3
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>


#include "file.h"
#include "interpreter.h"
#include "pinit.h"
#include "utils.h"


static void search_for_keyword(FILE *template, FILE *output, file *fl, int* linenumber);
static int check_for_word(file *fl, FILE *output, char* word);
static variable* check_if_variable_is_declared(node *variables, char *name);
static void handle_if_statement(FILE *link, FILE *output, file *fl, node** variables, int* linenumber);


/*
 * Processes the template file and based on the interpretation
 * it writes the content out to the outname of fl.
 */
void interpret_file(FILE *template, file *fl, node **variables)
{
	char current;
	int BUFFER_SIZE = 255;
	char buffer[BUFFER_SIZE];
	int linenumber = 1;

	//Open output in readmode to check if it exist.
	FILE *output = fopen(fl->outputname, "r");
	if(output) {
		if(NO_MAN){
			fclose(output);
			return;
		}
		else if(!YES_MAN) {
			printf("%s already exist, do you want to override it? [y/N]: ", fl->fullname);
			fgets(buffer, 19, stdin);
			if(buffer[0] != 'y' && buffer[0] != 'Y'){
				fclose(output);
				return;
			}
		}
	}

	//Open output in write mode
	output = fopen(fl->outputname, "w");
	if(!output){
		printf("FATAL: Did not manage to open outputstream!\n");
		printf("FATAL: Unable to open file: %s\n", fl->outputname);
		exit(1);
	}

	int length = 0;
	int status;
	while((status = fscanf(template, "%c", &current)) != EOF){
		// If character is start of an skip char.
		if(current == '\\'){
			buffer[length] = 0;
			fprintf(output, "%s", buffer);
			length = 0;
			buffer[length] = 0;
			status = fscanf(template, "%c", &current);
			if(current == ' ' || current == '\r' || current == '\n') {
				printf("Syntax Error: Uncompleted escape character at line line %d.\n", linenumber);
				printf("If you wanted to write a '\\' please write '\\\\'\n");
			}
			fprintf(output, "%c", current);

		}
		// If char start with $ this indicate the start of a keyword.
		else if(current == '$'){
			length = 0;
			buffer[0] = 0;
			search_for_keyword(template, output, fl, &linenumber);
		} 

		//If a space, newline, or carriage return is found try to evalutate word.
		else if(current == ' ' || current == '\r' || current == '\n'){
			if(current == '\n')
				linenumber++;
			buffer[length] = 0;
			length = 0;
			if(!strcmp(buffer, "IF")){
				handle_if_statement(template, output,  fl, variables, &linenumber);
			}
			else if(!strcmp(buffer, "EXT")){
				int ret = fscanf(template, "%254s", buffer);
				if(ret == 1) {
					variable *var = check_if_variable_is_declared(*variables, buffer);
					if(!var) {
						variable *v = malloc(sizeof(variable));
						v->name = malloc((strlen(buffer) + 1 ) * sizeof(char));
						strcpy(v->name, buffer);
						v->value = link_prompt(fl, buffer, variables);
					}
				} else {
					printf("Syntax error. Could not read extension on line %d\n", linenumber);
				}
			}
			else {
				fprintf(output, "%s", buffer);
				fprintf(output, "%c", current);
			}
			buffer[0] = 0;
		}
		else {
			buffer[length] = current;
			length++;
		}
	}

	fclose(output);
}


/*
 * As we have found a $ sign we are currently looking for
 * a keyword. Once we have found one we will check if it
 * is a known word.
 */
static void search_for_keyword(FILE *template, FILE *output, file *fl, int* linenumber)
{
	int searching = 1;
	int length = 0;
	int BUFFER_SIZE = 20;
	char buffer[BUFFER_SIZE -1];
	while(searching){
		int status = fscanf(template, "%c", &buffer[length]);
		if(status == EOF)
			break;
		char c = buffer[length];
		if(length == BUFFER_SIZE - 2){
			buffer[BUFFER_SIZE -1] = 0;
			fprintf(output, "%c%s", '$', buffer);
			searching = 0;
		}
		else {
			switch(c) {

				// Invalid use of $. Should use \$ instead.
				case '\0':
				case '\r':
				case '\n':
					buffer[length + 1] = 0;
					fprintf(output, "%c%s", '$', buffer);
					searching = 0;
					(*linenumber)++;
					break;

				// Found a $, try to check if the word exist.
				case '$':
					buffer[length] = 0;
					status = check_for_word(fl, output, buffer);
					if(!status){
						printf("Unknown word in template file for %s: %s\n", fl->extension, buffer);
					}
					searching = 0;
					break;
			}
		}
		length++;
	}
}


/*
 * This handles the logic incase an ifstatement is found.
 * If it finds another if inside it self, it will recursivly
 * call it self.
 *
 * This is a complicated function and should be refactored.
 * To the one is brave enough; May God have mercy upon your soul.
 */
static void handle_if_statement(FILE *link, FILE *output, file *fl, node** variables, int* linenumber)
{
	/*
	 * Status codes:
	 *
	 * 0 - Looking for extension
	 * 1 - Looking for THEN, AND, or OR
	 * 2 - Lokking for ELSE or DONE
	 * 3 - Looking for APPEND, ALSO, ELSE, or IF
	 */
	int status = 0;
	int expression = 0;
	char buffer[255];
	char current;
	int startline = *linenumber;
	int length = 0;

	/*
	 * Operators
	 *
	 * -1 - NO OPERATOR
	 * 0 - OR
	 * 1 - AND
	 */
	int operator = -1;
	int read_status = 0;
	while(read_status != EOF) {
		read_status = fscanf(link, "%c", &current);
		if(current == '\n' || current == ' ' || current == '\r' || current == '\t' || read_status == EOF){
			buffer[length] = 0;
			length = 0;

			//Just tried to read extension
			if(status == 0) {
				variable* var = check_if_variable_is_declared(*variables, buffer);
				if(var) {
					if(var->value){
						expression = 1;
						status = 1;
					}
					else
						status = 2;
				} else {
					int ret = link_prompt(fl, buffer, variables);
					var = malloc(sizeof(variable));
					var->name = malloc((strlen(buffer) + 1) * sizeof(char));
					strcpy(var->name, buffer);
					var->value = ret;
					add_element(variables, var);
					status = 1;

					if(operator == -1)
						expression = ret;
					else if(operator == 0)
						if(expression || ret)
							expression = 1;
						else
							expression = 0;
					else if(ret && expression) 
						expression = 1;
					else
						expression = 0;
				}
			}

			// Looking for a logical operator or
			// start of code block.
			else if(status == 1){
				if(!strcmp(buffer, "THEN")){
					// If value of expression is true
					// enter the if statement.
					if(expression)
						status = 3;
					// Else try to look for a else or done.
					else
						status = 2;
				}
				else if(!strcmp(buffer, "OR")) {
					operator = 0;
					status = 0;
				}
				else if(!strcmp(buffer, "AND")){
					operator = 1;
					status = 0;
				} else {
					printf("Syntax Error: Unexpected word %s at line %d in the %s template file.. Expected OR, AND, or THEN.\n", buffer, *linenumber, fl->extension);
					exit(1);
				}
			}

			// Looking for a else or done.
			// because of a false if evaluation.
			else if(status == 2){
				if(!strcmp(buffer, "ELSE"))
					status = 3;
				else if(!strcmp(buffer, "DONE")){
					if(current == '\t' || current == ' ')
						fprintf(output, "%c", current);
					return;
				}
			}

			// Evaluate code inside of an if statement.
			// Because of a true if statement.
			else if(status == 3) {
				if(!strcmp(buffer, "IF"))
					handle_if_statement(link, output, fl, variables, linenumber);
				else if(!strcmp(buffer, "ELSE"))
					status = 2;
				else if(!strcmp(buffer, "DONE")){
					if(current == '\t' || current == ' ')
						fprintf(output, "%c", current);
					return;
				}
				else if(!strcmp(buffer, "ALSO")){
					int ret = fscanf(link, "%254s", buffer);
					if(ret == 1)
						link_prompt(fl, buffer, variables);
					else {
						printf("Syntax Error. Could not read ALSO parameter\n");
					}
				}
				else if(!strcmp(buffer, "APPEND")) {
					current = 10;
					char read = 0;
					while(read != 2 && fscanf(link, "%c", &current) != EOF) {
						if(!read && current == '"')
							read = 1;
						else if(read && current == '"')
							read = 2;
						else if(read && current == '\n') {
							printf("Syntax Error: No \" after string.");
							read = 2;
						}
						else if(current == '$')
							search_for_keyword(link, output, fl, linenumber);
						else if(current == '\\'){
							int st = fscanf(link, "%c", &current);
							if(st == 1) {
								if(current == 'n')
									fprintf(output, "\n");
								else if(current == 'r')
									fprintf(output, "\r");
								else if(current == 't')
									fprintf(output, "\t");
								else
									fprintf(output, "%c", current);
							}
						}
						else if(read)
							fprintf(output, "%c", current);
					}
				}
			}

			// If the last char read was a new line
			// then we need to increase the linenumber.
			if(current == '\n')
				(*linenumber)++;
		} else {
			// Incase we have not found the end of the word
			// put it in the buffer.
			buffer[length] = current;
			length++;
		}
	}

	printf("Syntax Error: Missing DONE in if statement starting at line %d in the %s template file.\n", startline, fl->extension);
}


static variable* check_if_variable_is_declared(node *variables, char *name)
{
	while(variables){
		if(!strcmp(name, ((variable*)variables->data)->name))
			return (variable*)variables->data;
		variables = variables->next;
	}
	return NULL;	
}


static int check_for_word(file *fl, FILE *output, char* word)
{
	if(!strcmp(word, "BASENAME"))
		fprintf(output, "%s", fl->basename);
	else if(!strcmp(word, "UBASENAME"))
		fprint_upper(output, fl->basename);
	else if(!strcmp(word, "LBASENAME"))
		fprint_lower(output, fl->basename);
	else if(!strcmp(word, "EXTENSION"))
		fprintf(output, "%s",fl->extension);
	else if(!strcmp(word, "UEXTENSION"))
		fprint_upper(output, fl->extension);
	else if(!strcmp(word, "LEXTENSION"))
		fprint_lower(output, fl->extension);
	else if(!strcmp(word, "FULLNAME"))
		fprintf(output, "%s",fl->fullname);
	else if(!strcmp(word, "UFULLNAME"))
		fprint_upper(output, fl->fullname);
	else if(!strcmp(word, "LFULLNAME"))
		fprint_lower(output, fl->fullname);
	else if(!strcmp(word, "AUTHOR")){
		char *login = NULL;
		if((login = getlogin())) ;
		else if((login = getenv ("USER"))) ;
		else if ((login = getenv ("USERNAME")));
		else {
			printf("Warning: Unable to fetch username from OS\n");
		}
		if(login)
			fprintf(output, "%s", login);
	}
	else if(!strcmp(word, "DATE")){
		time_t t = time(NULL);
		struct tm tm = *gmtime(&t);	
		fprintf(output, "%02d/%02d/%04d", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900);
	}
	else {
		return 0;
	}
	return 1;
}


