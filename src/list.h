//created by prv on 10/12/2018

#ifndef LIST_H
#define LIST_H

typedef struct node_tag {
	struct node_tag *next;
	void *data;
} node;

void add_element(node **head, void *element);
void clear_list(node **head);
#endif
