/*
 *	File: utils.c
 *
 *	A file for collection of general functions which is
 *	uses many places and that do not have a good location.
 *
 *	Author: prv
 *	Date: 15/01/2019
 *
 */

#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <stdarg.h>

#include "utils.h"

static void _create_folder(char *path);

/*
 * Allocates memory for a new string which is a merge of the
 * three strings given.
 *
 * Returns a pointer to the first char.
 */

char* join_strings(int count, ...)
{

	size_t length = 0;
	va_list v, v_length;
	va_start(v, count);

	// Get the total length of the strings;
	va_copy(v_length, v);
	for(int i = 0; i < count; ++i) {
		length += strlen(va_arg(v_length, char*));
	}
	
	// Join the strings together
	char *string = malloc((length + 1) * sizeof(char));
	strcpy(string, va_arg(v, char*));
	for(int i = 1; i < count; ++i){
		strcat(string, va_arg(v, char*));
	}

	va_end(v);
	return string;
}
/*
 * Creates a folder with the given path and priviliges.
 * This function creates the folder then do all the error
 * handling if needed.
 */
void create_folder(char *path)
{
	int index = 0;
	while(1){
		if(path[index] == '/' || path[index] == 0){
			char ch = path[index];
			path[index] = 0;
			_create_folder(path);
			path[index] = ch;
			if(!path[index] || !path[index + 1])
				break;
		}
		index++;
	}
}

static void _create_folder(char *path)
{
	int status = mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	if(status == -1){
		if(errno == EACCES)
			printf("Error: Do not have permission to create folder '%s'\n", path);
		else if(errno == EEXIST) {
			struct stat st;
			if(!stat(path, &st) && S_ISDIR(st.st_mode))
				return;
			printf("Error: The file %s already exist\n", path);
		}
		else if(errno == ENOENT)
			printf("Error: Part of the path does not exist, %s\n", path);
		else if(errno == EROFS)
			printf("Error: The parent directory of %s is in a read-only file system.\n", path);
		else
			printf("Error: Could not make folder '%s' for unknown reason.\n", path);
		exit(1);
	}
}

/*
 * Prints the given string in all uppercase letters.
 * The file needs to be open.
 */
void fprint_upper(FILE *output, char *c)
{
	while(*c){
		fprintf(output, "%c", toupper(*c));
		c++;
	}
}


/* 
 * Prints the given string in all lowercase letters.
 * The file needs to be open.
 */
void fprint_lower(FILE *output, char *c)
{
	while(*c){
		fprintf(output, "%c", tolower(*c));
		c++;
	}
}



