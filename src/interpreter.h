/*
 *	File: interpreter.h
 *	
 *	Author: prv
 *	Date: 22/01/2019
 *
 */

#ifndef TEMPLATE_H
#define TEMPLATE_H

/* 
 * Variable to hold a variable and its
 * current value.
 */
typedef struct variable_s variable;
struct variable_s {
	char* name;
	int value;
};

void interpret_file(FILE *template, file *fl, node **variables);

#endif

