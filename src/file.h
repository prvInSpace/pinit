//created by prv on 10/12/2018

#ifndef FILE_H
#define FILE_H

#include "list.h"

typedef struct file_tag {
	char *template_path;
	char *path;
	char *basename;
	char *extension;
	char *type;
	char *fullname;
	char *outputname;
} file;

void create_files(node *files);
void create_linked_file(file *fl, char* buffer, node **variables);
int check_if_file(char *argv, node **files);
int link_prompt(file *fl, char* buffer, node **variables);

#endif
