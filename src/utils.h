//created by prv on 15/01/2019

#ifndef UTILS_H
#define UTILS_H

char* join_strings(int count, ...);
void create_folder(char* path);
void fprint_upper(FILE *output, char *c);
void fprint_lower(FILE *output, char *c);

#endif
