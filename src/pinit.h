//created by prv on 09/12/2018

#ifndef PINIT_H
#define PINIT_H

#include "list.h"

extern char *MAKEFILES;
extern char *TEMPLATES;
extern char *CONFIGPATH;
extern char *PROJECT_FOLDER;
extern char *DEFAULT_PROJECT_FOLDER;
extern int YES_MAN;
extern int NO_MAN;
extern int DEFAULT_ONLY;

#endif
