/*
 *	File: main.c
 *	
 *  Main file to use when programming for matrix formula all code buggy
 *
 *	Author: Sindre Aalhus
 *	Date: 29/01/2019
 *
 */

#include "allcode_api.h"

int main()
{
	FA_RobotInit();

	return 0;
}
