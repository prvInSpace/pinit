# Pinit
[![licence](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/prvInSpace/pinit/blob/master/LICENSE)
[![pipeline status](https://gitlab.com/prvInSpace/pinit/badges/master/pipeline.svg)](https://gitlab.com/prvInSpace/pinit/commits/master)

Pinit is an application that creates a new empty project for the supported
languages and project types. It can also be used to create files based on
templates. Currently the project contains Makefiles and Gitignore files for
Assembly, C, C++, Fortran, and Haskell. 

If you would like to contribute, please feel free to read the section about
contributing to the project.

N.B: Assembly and Fortran support is relativly new. This mean that some
features are missing or broken, but we do not currently know of any problems.
If you find any bugs, please feel free to send in an issue.

## Installation and Uninstallation

The makefile for Pinit contains a function for installation and one for
uninstalling. To install Pinit go to the root folder and write "make install".
For uninstallation please write "make uninstall".

## How to Use

pinit [LANGUAGE] [FOLDER]

To create a project simply type "pinit" followed by the language code and a folder.
The folder is optional, and if a folder is ommited pinit will simply use the current
directory. Pinit is not bundled with any compilers, and these needs to be downloaded
seperatly. If you use another compiler than the default, this is often very easy to
change.

pinit [FILENAME] ...

To create a file based on a template, write "pinit" followed by the name of the file
with an extension. It will then try to find a template with the given extension.
You are able to write as many files you want


## Supported Projects and Languages

* C [c]
* C++ [cpp]
* Fortran [fortran]
* GAS (AT&T) Assembly Language [gas]
* Haskell [haskell]
* Matrix Forumla AllCode Robots [matrix]
* NASM Assembly Language [nasm]

## Supported File Templates

* C File [.c]
* C++ File [.cpp]
* C/C++ Header [.h]
* Fortran Free-form File [.f90, .f95, .f03] 
* GAS Assembly File [.s]
* Haskell File [.hs]
* HTML File [.html]
* NASM Assembly File. [.asm]

## Pinit Template Language

Pinit contains a full template definition language with a lot of keywords which
will be evaluated and changed out upon creation. It also contains IF statements
and logical operators for linking different file extensions and appending text.
If you want more information about the Pinit Template Language, please refere to
the markdown document in the folder docs, called PTL_definition.md.

## Overriding or Creation of Templates

By creating the folders "makefiles" and "templates" in ~/.config/pinit/ will let
you override the default templates. This is done on a folder for folder basis,
and a "c" folder in .config/pinit will override the entire content of the default
c folder. By adding your own folders you are simply creating your own custom
templates.

## Release Notes

A general overview can be found the Changelog file. If you want a more "bloggy"
release notes you can find those at http://prv.space/pinit

## Pull Requests and Issues

I am open for any issues and pull requests. If you want to help out by creating
a general purpose Makefile, please make sure it follows the structure as the
other languages. If you make a pull requests which is approved, you will be mentioned
under credits.

## Contributing

Feel free to contribute in any way you want to. Everything from bug-reporting, adding
support for new languages, or improve the documentation. Any merge requests should be
sent to the development branch, where they will be reviewed by any maintainer or
developer. We welcome any contributions, everything from code to documentation.

## Distrubution

You are free to modify and share the program as you see fit. I would really appriciate
if you would give back to the community by sending the changes back here.

## Maintainers

* Lead Maintainer: Preben Vangberg | prvInSpace
* Second-In-Command: Sindre Aalhus | siaInSpace

## Contributors

* rur7
* theSlask

