#
# General purpose Makefile for C.
# 
# The Makefile is licensed under MIT
# and is part of the Pinit project.
#
# Author: Preben Vangberg
# Since: 21.10.2018 
#

OS = Linux

VERSION = 1.1.0
CC	= gcc

TARGET := bin/main
PARAM := -Wall -Wstrict-prototypes -Wmissing-prototypes -Wshadow -Wconversion

SRCDIR := src
OBJDIR := obj

SRCS := $(shell find $(SRCDIR) -name "*.c")
HDRS := $(shell find $(SRCDIR) -name "*.h")
OBJS := $(SRCS:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

$(TARGET): $(OBJS)
	@mkdir -p bin
	$(CC) -o $@ $^ $(EXTRAPARAM)


$(OBJDIR)/%.o: $(SRCDIR)/%.c $(HDRS)
	@mkdir -p obj
	$(CC) $(PARAM) $(@:$(OBJDIR)/%.o) -o $@ -c $<


.PHONY : all clean install uninstall

clean: 
	rm -f $(OBJDIR)/*.o
	
all: clean $(TARGET)

install:
	@echo Installing pinit!
	@make all > /dev/null
	@echo Linking makefiles...
	@sudo mkdir -p /usr/share/prv/
	@sudo ln -f -s $(shell pwd)/makefiles /usr/share/prv
	@sudo ln -f -s $(shell pwd)/templates /usr/share/prv
	@echo Linking binaries...
	@sudo ln -f -s $(shell pwd)/bin/main /usr/bin/pinit
	@echo Setting up man pages...
	@sudo install -g 0 -o 0 -m 0644 docs/pinit_man /usr/share/man/man1/pinit.1
	@sudo gzip -f /usr/share/man/man1/pinit.1
	@sudo mandb -q
	@echo Done!

uninstall:
	@echo "Uninstalling pinit!"
	@sudo rm -rf /usr/bin/pinit
	@sudo rm -rf /usr/share/prv/makefiles
	@sudo rm -rf /usr/share/prv/templates
	@sudo rm -rf /usr/share/man/man1/pinit.1
	@sudo rm -rf /usr/share/man/man1/pinit.1.gz
	@sudo mandb -q
	@echo "Done!"

