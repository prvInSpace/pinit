
# The Pinit Template Language

Welcome to the documentation sheet for the Pinit Template Language. This is
meant to be the main reference sheet for the language, syntax, and any
available keywords.

## Table of Contents
1. [Syntax](#syntax)
2. [Reserved Words](#reserved_words)
3. [Keywords](#keywords)

## Syntax <a name="syntax" />

### Start of a Codeblock

A code block can start with one of two reserved words. These
are EXT and IF. EXT is used when you want to link to another extension
without having it inside an IF statement. IF is the same
as in most other languages the start of an IF statement.

Keywords can be used anyplace, and does not have to be inside a
codeblock.

Inside a codeblock no spaces or newlines will be printed unless stated
inside a APPEND string. All reserved words and parameters needs to be
seperated by a space, tab, or newline. 

## End of a Codeblock.

EXT does not have an end, but the first word afterwards is considered
a parameter. The IF statement block needs to be finished of by DONE.

### Logical Operators

The language contains to logical operators. These are And and Or. Serve work
as any other logical operators in other languages. As the language does not
have any brackets the operators works on the values on the right and left,
and it will solve any linked operators from left to right. This means that
if you have the if statement ```IF a AND b OR c THEN``` it will look like the
C code ```if((a && b) || c) {}```.   

### Example Code
#### Standard Link
~~~
/* 
 * Author: $AUTHOR$
 */

EXT a
~~~

This template will create a header comment with the name of the author of the file.
It will also ask the user if it wants a file with the extension a.

#### Appending Text
~~~
// $DATE$

IF a THEN
	APPEND "#include \"$BASENAME$.a\""
DONE
~~~ 
This will add the current date to the top, then as the user if it want a file with
the extension a. If the user answers yes, we will append the text #include "file.a"
to the current file.

## Reserved Words <a name="reserved_words" />

This is a list of all the reserved words in the language. If you want to write
these inside the template you should prefix it with a \ to make sure the parser
skips it.

* ALSO          - Prompt for the creation of the given extension.
* APPEND        - Appends text to the current template.
* DONE          - Marks the end of a codeblock.
* ELSE          - Start the else block of the if statement.
* EXT           - Ask for the creation of a extension outside a if statement.
* IF            - Starts an if-statement.
* THEN          - Starts the if block of the if statement.

## Keywords <a name="keywords" />

The template language contains a lot of keywords which will be replaced when
the file is created. Most of these also have a uppercase and lowercase
version. All keywords needs to be encircled with '$' signs.

The upper and lowercase versions can be accessed by prefixing a 'U' or 'L'
in front of the keyword like this: '$UBASENAME$'. This will print out the
basename of the file in uppercase letters.

* BASENAME      - The basename of the file.
* EXTENSION		- The extension of the file
* FILENAME		- The entire filename.
* AUTHOR		- The author of the file
* DATE 			- Inserts the current date

